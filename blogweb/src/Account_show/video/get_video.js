import video from '../../api/video'


const get_video = () => {
    video.get_video().then(response=> {
        video.video_list = response.data
        video.copy_video_list = video.video_list
        for(let item in video.video_list) {
            video.video_list[item].publish_time = video.video_list[item].publish_time.slice(0, 10)
            video.copy_video_list[item].publish_time = video.video_list[item].publish_time.slice(0, 10)
        }
    })
}

export default get_video