import follow from '../../api/follow'


const get_follow = () => {
    let token = sessionStorage.token || localStorage.token
    follow.get_follow(token).then(response=>{
        follow.follow_list = response.data
    })
}

export default get_follow
