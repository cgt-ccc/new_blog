import http from "../../utils/http.js"
import {reactive} from "vue"
import update_user from "../../api/update_user.js"
import {ElMessage} from "element-plus";

const update_password_handle = () => {
    if (update_user.password == "") {
        ElMessage.error("密码不能为空")
    }
    update_user.update_password().then(response => {
        window.setTimeout(function () {
              window.location.reload();
        },500)
        ElMessage.success("修改密码成功！")
    }).catch(err => {
        if (err.response.data.errmsg[0]) {
            ElMessage.error(err?.response?.data?.errmsg[0])
        }
    })
}


export default update_password_handle
