import update_user from "../../api/update_user.js"
import store from "../../store/index.js";
import {ElMessage} from "element-plus";


const update_mobile_handle = () => {
    if (update_user.mobile == "") {
        ElMessage.error("手机号不能为空")
        return false
    }
    update_user.update_mobile().then(response => {
        store.commit("update_mobile", update_user.mobile)
         // 重新加载页面
        window.setTimeout(function () {
              window.location.reload();
        },500)
        ElMessage.success("修改手机号成功！")
    }).catch(err => {
        if (err.response.data.mobile[0] == "用户信息 with this 手机号 already exists.") {
            ElMessage.error("该手机号已存在")
            return false
        }
        ElMessage.error(err?.response?.data?.mobile[0])

    })
}


export default update_mobile_handle


