import update_user from "../../api/update_user.js";
import store from "../../store/index.js";
import {ElMessage} from "element-plus";

const update_email_handle = () => {
    // update_user.update_email().then(response => {
    //     if (store.state.user.email == "") {
    //         ElMessage.success("添加成功！")
    //     }
    //     else {
    //         ElMessage.success("修改成功！")
    //     }
    //     store.commit('update_email', update_user.email)
    //     update_user.email = store.state.user.email
    //     // 重新加载页面
    //     window.setTimeout(function () {
    //           window.location.reload();
    //     },500)
    // }).catch(err => {
    //
    // })
    update_user.update_email().then(response => {
        if (store.state.user.email == "") {
            ElMessage.success("添加成功！")
        }
        else {
            ElMessage.success("修改成功！")
        }
        store.commit('update_email', update_user.email)
        update_user.email = store.state.user.email
        // 重新加载页面
        window.setTimeout(function () {
              window.location.reload();
        },500)
      }).catch(err => {
          if (err?.response?.data?.email[0] == "邮箱不能为空") {
              ElMessage.error(err.response.data.email[0])
          }
          else if (err?.response?.data?.email[0] == "Enter a valid email address.") {
              ElMessage.error("请输入有效的邮箱地址")
          }
      })
}


export default update_email_handle


