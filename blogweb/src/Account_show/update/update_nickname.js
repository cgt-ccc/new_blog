import update_user from "../../api/update_user.js";
import {ElMessage} from "element-plus";
import store from "../../store/index.js";

const update_nickname_handle = () => {
  if (update_user.nickname == "") {
    ElMessage.error("用户名不能为空")
    return false
  }
  if (!/[\u4e00-\u9fa5a-zA-Z]+$/.test(update_user.nickname)) {
    // 错误提示
    ElMessage.error('用户名格式错误')
    return false // 阻止代码继续往下执行
  }

  // 修改用户名
  update_user.update_nickname().then(response => {
    store.commit('update_nickname', update_user.nickname)
    // update_user.nickname = store.state.user.nickname
    // 重新加载页面
    window.setTimeout(function () {
          window.location.reload();
    },500)
    ElMessage.success("修改用户名成功！")
  }).catch(err => {
      if (err?.response?.data?.errmsg) {
        ElMessage.error(err?.response?.data?.errmsg[0])
    }
  })
}

export default update_nickname_handle