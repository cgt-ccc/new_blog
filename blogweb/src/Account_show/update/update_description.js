import update_user from "../../api/update_user.js"
import {reactive} from "vue"
import store from "../../store/index.js";
import {ElMessage} from "element-plus";


const update_description_handle = () => {
    if(update_user.description == "") {
        ElMessage.error("自我介绍不能为空")
        return false
    }
    update_user.update_description().then(response => {
        store.commit('update_description', update_user.description)
        window.setTimeout(() => {
            window.location.reload()
        },500)
        ElMessage.success("成功")
    }).catch(err => {
        ElMessage.error(err?.response?.data?.description[0])
    })
}

export default update_description_handle