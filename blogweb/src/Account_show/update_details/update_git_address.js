import {reactive} from "vue";
import user_details from "../../api/user_details.js";
import {ElMessage} from "element-plus";


const update_git_address_handle = () => {
    if(user_details.git_address == "") {
        ElMessage.error("github地址不能为空！")
        return false
    }
    user_details.update_git_address().then(response => {
        window.setTimeout(() => {
            window.location.reload()
        }, 500)
        ElMessage.success("成功")
    }).catch(err => {
        ElMessage.error(err?.response?.data?.git_address[0])
    })
}

export default update_git_address_handle