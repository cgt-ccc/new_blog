import user_details from "../../api/user_details.js";
import {ElMessage} from "element-plus";


const create_details_handle = () => {
    if(user_details.industry=="" || user_details.git_address=="" || user_details.content_direction=="") {
        ElMessage.error("以下内容都不能为空！")
        return false
    }
    user_details.create_details().then(response => {
        ElMessage.success("添加成功")
        window.setTimeout(() => {
            window.location.reload()
        }, 500)
    })
}


export default create_details_handle