import user_details from "../../api/user_details.js";
import {ElMessage} from "element-plus";

const update_industry_handle = () => {
  if(user_details.industry == "") {
     ElMessage.error("所在行业不能为空！")
    return false
  }
  user_details.update_industry().then(response => {
    // 重新加载页面
    window.setTimeout(function () {
          window.location.reload();
    },500)
    ElMessage.success("成功")
  }).catch(err => {
    ElMessage.error(err?.response?.data?.industry[0])
  })
}

export default update_industry_handle