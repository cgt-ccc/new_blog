import {reactive} from "vue";
import user_details from "../../api/user_details.js";
import {ElMessage} from "element-plus";



const update_content_direction_handle = () => {
    if(user_details.content_direction == "") {
        ElMessage.error("内容方向不能为空！")
        return false
    }
    user_details.update_content_direction().then(response => {
        window.setTimeout(() => {
            window.location.reload()
        }, 500)
        ElMessage.success("成功")
    })
}

export default update_content_direction_handle