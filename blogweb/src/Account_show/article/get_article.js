import article from "../../api/article.js"
const get_article = () => {
    article.get_article().then(response => {
      // console.log(route.name)
      article.article_list = response.data
      article.copy_article_list = article.article_list
      for(let item in article.article_list) {
        article.article_list[item].publish_time = article.article_list[item].publish_time.slice(0, 10)
        article.copy_article_list[item].publish_time = article.copy_article_list[item].publish_time.slice(0, 10)
      }
    })
}

export default get_article
