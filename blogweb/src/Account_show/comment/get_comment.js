import article from '../../api/article'

const get_comment = () => {
    article.get_comment().then(response => {
        article.comment_list = response.data
        for(let item in article.comment_list) {
            article.comment_list[item].comment_time = article.comment_list[item].comment_time.slice(0, 10)
        }
    })
}


export default get_comment