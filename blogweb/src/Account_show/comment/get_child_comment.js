import article from "../../api/article.js";


const get_child_comment = () => {
    article.get_child_comment().then(response => {
        // console.log(response.data)
        article.child_comment_list = response.data
        for(let item in article.child_comment_list) {
            article.child_comment_list[item].comment_time = article.child_comment_list[item].comment_time.slice(0, 10)
        }
    })
}

export default get_child_comment