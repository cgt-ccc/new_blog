import music from '../../api/music'
const get_music = () => {
    // let token = sessionStorage.token || localStorage.token
    music.get_music().then(response=> {
        music.music_list = response.data
        music.copy_music_list = music.music_list
        for(let item in music.music_list) {
            music.music_list[item].publish_time = music.music_list[item].publish_time.slice(0, 10)
            music.copy_music_list[item].publish_time = music.music_list[item].publish_time.slice(0, 10)
        }
    })
}

export default get_music