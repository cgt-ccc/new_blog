import {createApp, reactive, ref} from 'vue'
import './style.css'
import router from "./router/index.js";
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import './assets/iconfonts/iconfont.css'
import './assets/iconfonts/iconfont'
import 'element-plus/dist/index.css'
import store from "./store/index.js"

import VuemarkdownEditor from '@kangc/v-md-editor'
import '@kangc/v-md-editor/lib/style/base-editor.css';
import vuepressTheme from '@kangc/v-md-editor/lib/theme/vuepress.js';
import '@kangc/v-md-editor/lib/theme/style/vuepress.css';
import Prism from 'prismjs';


// 全局获取文章列表
import get_article from "./Account_show/article/get_article.js";

get_article()

VuemarkdownEditor.use(vuepressTheme, {
  Prism,
});

createApp(App).use(VuemarkdownEditor).use(router).use(store).mount('#app')