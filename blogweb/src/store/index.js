import {createStore} from "vuex"
import createPersistedState from "vuex-persistedstate"

// 实例化一个vuex存储库
export default createStore({
    plugins: [createPersistedState()],
    state () {  // 数据存储位置，相当于组件中的data
        return {
          user: {

          },
            details: {

            },
            isExpend: false,
            child_comment_list: []
        }
    },
    getters: {
        getUserinfo(state) {
            let now = parseInt(new Date().getTime()/1000)
            if (state.user.exp == undefined) {
                state.user = {}
                // localStorage.token = null
                // sessionStorage.token = null
                return null
            }
            if (state.user.exp < now) {
                state.user = {}
                // localStorage.token = null
                // sessionStorage.token = null
                return null
            }
            return state.user
        }
    },
    mutations: { // 操作数据的方法，相当于methods
        login(state, user) {  // state 就是上面的state   state.user 就是上面的数据
            state.user = user
        },
        logout(state) {
            state.user = {}
            localStorage.token = null
            sessionStorage.token = null
        },
        update_nickname(state, nickname) {
            state.user.nickname = nickname
        },
        // vuex不存储密码，因此不需要进行更改操作
        update_email(state, email) {
            state.user.email = email
        },
        update_mobile(state, mobile) {
            state.user.mobile = mobile
        },
        update_description(state, description) {
            state.user.description = description
        },
        update_avatar(state, avatar) {
            state.user.avatar = avatar
        },
        // 文章详情页
        get_article_details(state, article_item) {
            state.details = article_item
        },
        // // 删除
        // delete_article_details(state, index) {
        //     state.details = state.details.slice(index, 1)
        // },
        get_article_list_length(state, article_length) {
            state.details.length = article_length
        },
        // 传递判断值是否伸展
        get_isExpend(state, isExpend) {
            state.isExpend = isExpend
        },
        get_child_comment(state, child_comment_list) {
            state.child_comment_list = child_comment_list
        }
    }
})