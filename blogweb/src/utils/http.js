import axios from "axios"

const http = axios.create({
    baseURL: "http://api.blog.cn:8000",
    withCredentials: false,      // 是否允许客户端ajax请求时携带cookie
})

// config 是一个包含请求配置的对象，比如请求头、请求体、URL 等信息
// 请求拦截器
http.interceptors.request.use((config) => {
    return config
}, (error) => {
    console.log("http请求错误")
    // 该返回的数据则是axios.catch(err)中接收的数据
    return Promise.reject(error)
})

// 相应拦截器
http.interceptors.response.use((response) => {
    // console.log("服务端响应数据成功以后，返回结果给客户端的第一时间，执行then之前")
    // 该返回的数据则是axios.then(res)中接收的数据
    return response
}, (error) => {
    // console.log("服务器相应失败时")
    // 该返回的数据则是axios.catch(err)中接收的数据
    return Promise.reject(error)
})

export default http