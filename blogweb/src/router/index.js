import { createRouter, createWebHistory} from 'vue-router'
import store from "../store/index"
import {ElMessage} from "element-plus";


const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/Home',
            name: 'Home',
            component: () => import('../views/Home.vue'),
            meta: {
                title: "blog首页",
                keepalive: true,
            },
        },
        {
            path: '/Login',
            name: 'Login',
            component: () => import('../views/Login.vue'),
            meta: {
                title: "用户登录页面",
                keepalive: true
            }
        },
        {
            path: '/Register',
            name: 'Register',
            component: () => import('../views/Register.vue'),
            meta: {
                title: "用户注册页面",
                keepalive: true
            }
        },
        {
            path: '/Learn',
            name: 'Learn',
            component: () => import('../views/Learn.vue'),
            meta: {
                title: "blog学习心得页面",
                keepalive: true
            }
        },
        {
            path: '/Discuss',
            name: 'Discuss',
            component: () => import('../views/Discuss.vue'),
            meta: {
                title: "blog讨论页面",
                keepalive: true
            }
        },
        {
            path: '/dev_info',
            name: 'Dev_info',
            component: () => import('../components/development_info/help/Dev_info.vue'),
            meta: {
                title: "联系开发者",
                keepalive: true
            }
        },
        {
            path: '/reporting_minors',
            name: 'Reporting_by_minors',
            component: () => import('../components/development_info/report/Reporting_by_minors.vue'),
            meta: {
                title: "未成年举报",
                keepalive: true
            }
        },
        {
            path: '/reporting_online_rumor',
            name: 'Reporting_by_online_rumor',
            component: () => import('../components/development_info/report/Reporting_by_online_rumor.vue'),
            meta: {
                title: "网络谣言举报",
                keepalive: true
            }
        },
        {
            path: '/setting/Account',
            name: 'Account',
            component: () => import('../views/Setting_pages/Account.vue'),
            meta: {
                title: "账户与密码设置页面",
                keepalive: true,
                authorization: true
            }
        },
        {
            path: '/setting/Notification',
            name: 'Notification',
            component: () => import('../views/Setting_pages/Notification.vue'),
            meta: {
                title: "消息通知中心",
                keepalive: true,
                authorization: true
            }
        },
        {
            path: '/setting/Details',
            name: 'Details',
            component: () => import('../views/Setting_pages/Details.vue'),
            meta: {
                title: "详细资料设置页面",
                keepalive: true,
                authorization: true
            }
        },
        {
            path: '/Home/Article',
            name: 'Article',
            component: () => import('../components/home_components/article.vue'),
            meta: {
                title: "发布文章",
                keepalive: true,
                // authorization: true
            }
        },
        {
            path: '/Home/Article_details',
            name: 'Article_details',
            component: () => import('../components/home_components/article_details.vue'),
            meta: {
                title: "Article_details",
                keepalive: true,
            }
        },
        {
            path: '/Home/Article_Update',
            name: 'Article_Update',
            component: () => import('../components/home_components/article_update.vue'),
            meta: {
                title: "Article_Update",
                keepalive: true,
            }
        },
        {
            path: '/user/user_article',
            name: 'User_article',
            component: () => import('../views/user_pages/article.vue'),
            meta: {
                title: "User_article",
                keepalive: true,
            }
        },
        {
            path: '/user/user_collect',
            name: 'User_collect',
            component: () => import('../views/user_pages/collect.vue'),
            meta: {
                title: "User_collect",
                keepalive: true,
            }
        },
        {
            path: '/user/user_music',
            name: 'User_music',
            component: () => import('../views/user_pages/music.vue')
        },
        {
            path: '/user/user_music_collect',
            name: 'User_music_collect',
            component: () => import('../views/user_pages/music_collect.vue')
        },
        {
            path: '/user/user_video',
            name: 'User_video',
            component: () => import('../views/user_pages/video.vue')
        },
        {
            path: '/user/user_video_collect',
            name: 'User_video_collect',
            component: () => import('../views/user_pages/video_collect.vue')
        },
        {
            path: '/Chats',
            name: 'Chats',
            component: () => import('../views/chats.vue'),
            meta: {
                title: "Chats",
                keepalive: false,
                authorization: true
            }
        },
        {
            path: '/Find_article',
            name: 'article',
            component: () => import('../views/Find_category/Find_article.vue')
        },
        {
            path: '/Find_video',
            name: 'video',
            component: () => import('../views/Find_category/Find_video.vue')
        },
        {
            path: '/Find_game',
            name: 'game',
            component: () => import('../views/Find_category/Find_game.vue')
        },
        {
            path: '/Find_music',
            name: 'music',
            component: () => import('../views/Find_category/Find_music.vue')
        },
        {
            path: '/user/user_info_article',
            name: 'user_info_article',
            component: () => import('../components/other_user/user_info_article.vue')
        },
        {
            path: '/user/user_info_collect',
            name: 'user_info_collect',
            component: () => import('../components/other_user/user_info_collect.vue')
        },
        {
            path: '/Follow',
            name: 'Follow',
            component: () => import('../views/Follow.vue')
        },
    ]
})


// 导航守卫
router.beforeEach((to, from, next)=>{
  document.title=to.meta.title
  // 登录状态验证
  if (to.meta.authorization && !store.getters.getUserinfo) {
      ElMessage.error("您尚未登录，不能进入该页面页面")
    next({"name": "Home"})
  }
  else{
    next()
  }
})

export default router
