import http from '../utils/http'
import { reactive } from "vue";
import store from "../store/index.js";

const music = reactive({
    music_name: "",
    music_id: "",
    collect_list: "",
    music_file: "",
    // music_picture: "", 这里使用和article封面相同数据
    music_author: store.state.user.user_id,
    music_list: [],
    copy_music_list: [],
    create_music(formData, token) {
        return http.post('/users/create_music/', formData, {
            headers: {
                Authorization: "jwt " + token
            }
        })
    },
    get_music() {
        return http.get('/users/get_music/')
    },
    update_collect_list(token) {
        return http.put(`/users/update_music_collect/${this.music_id}/`, {
            'collect_list': this.collect_list
        }, {
                headers: {
                    Authorization: "jwt " + token,
                }
            })
    },
    delete_music(token) {
        return http.delete(`/users/delete_music/${this.music_id}/`, {
              headers: {
                    Authorization: "jwt " + token,
              }
        })
    }
})

export default music