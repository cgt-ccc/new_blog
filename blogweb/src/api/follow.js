import http from "../utils/http.js"
import { reactive } from "vue"


const follow = reactive({
    user_id: "",
    follow_list: [],
    create_follow(token) {
        return http.post('/users/follow/', {
            'user': this.user_id
        }, {
            headers: {
                Authorization: "jwt " + token
            }
        })
    },
    get_follow(token) {
        return http.get('/users/get_follow/', {
            headers: {
                Authorization: "jwt " + token
            }
        })
    },
    delete_follow() {
        return http.delete(`./users/delete_follow/${this.user_id}/`)
    }
})

export default follow