import http from "../utils/http.js"
import { reactive } from "vue"
import { useStore } from "vuex";
import store from "../store/index.js"

const user_details = reactive({
    user: store.state.user.user_id,
    details_list: [],
    industry: "",
    content_direction: "",
    git_address: "",
    get_details() {
        return http.get(`users/details/${this.user}/`)
    },
    create_details() {
        return http.post(`users/create_details/${this.user}/`, {
            'user': this.user,
            'industry': this.industry,
            'content_direction': this.content_direction,
            'git_address': this.git_address
        })
    },
    update_industry() {
        return http.put(`users/update_industry/${this.details_list.id}/`, {
            "industry": this.industry
        })
    },
    update_content_direction() {
        return http.put(`users/update_content_direction/${this.details_list.id}/`, {
            "content_direction": this.content_direction
        })
    },
    update_git_address() {
        return http.put(`users/update_git_address/${this.details_list.id}/`, {
            "git_address": this.git_address
        })
    }
})

export default user_details