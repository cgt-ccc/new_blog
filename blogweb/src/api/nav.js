import http from '../utils/http'
import { ref, reactive } from "vue"


const nav = reactive({
    header_nav_list: [],    // 导航栏列表
    login_nav_list: [],
    develop_nav_list: [],
    get_header_nav() {
        return http.get("/home/nav/header/")
    }
})

export default nav