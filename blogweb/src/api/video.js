import http from '../utils/http'
import { reactive } from "vue";
import store from "../store/index.js";

const video = reactive({
    video_name: "",
    video_id: "",
    collect_list: "",
    video_file: "",
    // music_picture: "", 这里使用和article封面相同数据
    video_author: store.state.user.user_id,
    video_list: [],
    copy_video_list: [],
    create_video(formData, token) {
        return http.post('/users/create_video/', formData, {
            headers: {
                Authorization: "jwt " + token
            }
        })
    },
    get_video() {
        return http.get('/users/get_video/')
    },
    update_collect_list(token) {
        return http.put(`/users/update_video_collect/${this.video_id}/`, {
            'collect_list': this.collect_list
        }, {
                headers: {
                    Authorization: "jwt " + token,
                }
            })
    },
})

export default video