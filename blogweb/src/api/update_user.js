import http from '../utils/http'
import { reactive } from "vue";
import store from "../store/index.js";

const update_user = reactive({
    nickname: "",
    password: "",
    email: "",
    mobile: "",
    user: store.state.user.user_id,
    description: "",
    update_nickname() {
        return http.put(`users/update_nickname/${this.user}/`, {
             "nickname": this.nickname
        })
    },
    update_password() {
        return http.put(`users/update_password/${this.user}/`, {
            "password": this.password
        })
    },
    // update_email() {
    //     return http.put(`users/update_email/${this.user}/`), {
    //         "email": this.email
    //     }
    // },
    update_email() {
        return http.put(`users/update_email/${this.user}/`, {
            "email": this.email
        })
    },
    update_mobile() {
        return http.put(`users/update_mobile/${this.user}/`, {
            "mobile": this.mobile
        })
    },
    update_description() {
        return http.put(`users/update_description/${this.user}/`, {
            "description": this.description
        })
    },
    update_avatar(formData) {
        return http.put(`users/update_avatar/${this.user}/`, formData)
    }
})

export default update_user
