import http from '../utils/http'
import { reactive } from "vue";
import store from "../store/index.js"

const article = reactive({
    title: "",
    content: "",
    publish_time: "",
    category: "",
    author: store.state.user.user_id,
    picture: null,
    article_list: [],
    copy_article_list: [],
    article_id: "",
    collect_list: "",
    comment: "",
    comment_list: "",
    child_comment_list: "",
    comment_article_id: "",
    comment_author: "",
    pre_comment: null,
    reply: "",
    reply_child: "",
    get_article() {
        return http.get('/users/show_article/')
    },
    create_article(formData, token) {
        return http.post('/users/article/', formData, {
             headers: {
                    Authorization: "jwt " + token,
             }
        })
    },
    update_article(formData, token) {
        return http.put(`/users/update_article/${this.article_id}/`, formData, {
            headers: {
                    Authorization: "jwt " + token,
            }
        })
    },
    delete_article(token) {
        return http.delete(`/users/update_article/${this.article_id}/`, {
            headers: {
                Authorization: "jwt " + token,
            }
        })
    },
    update_collect_list(token) {
        return http.put(`/users/update_collect_list/${this.article_id}/`, {
            'collect_list': this.collect_list
        }, {
                headers: {
                    Authorization: "jwt " + token,
                }
            })
    },
    get_collect_list() {
        return http.get(`/users/update_collect_list/${this.article_id}/`)
    },
    get_look_num() {
        return http.get(`/users/get_look_num/${this.article_id}/`)
    },
    add_look_num(look, token) {
        return http.put(`/users/add_look_num/${this.article_id}/`, {
            'look': look
        }, {
            headers: {
                Authorization: "jwt " + token
            }
        })
    },
    get_comment() {
        return http.get(`/users/create_comment/`)
    },
    get_child_comment() {
        return http.get(`/users/get_child_comment/`)
    },
    create_child_comment(token) {
        return http.post(`/users/create_child_comment/`, {
            'comment_content': this.comment,
            'comment_author': this.comment_author,
            'article': this.comment_article_id,
        }, {
            headers: {
                Authorization: "jwt " + token
            }
        })
    },
    reply_comment(token) {
        return http.post(`/users/reply_comment/`, {
            'comment_content': this.reply,
            'comment_author': this.comment_author,
            'article': this.comment_article_id,
            'pre_comment': this.pre_comment
        }, {
            headers: {
                Authorization: "jwt " + token
            }
        })
    },
    // 使用同一个回复
    reply_child_comment(token) {
        return http.post(`/users/reply_comment/`, {
            'comment_content': this.reply_child,
            'comment_author': this.comment_author,
            'article': this.comment_article_id,
            'pre_comment': this.pre_comment
        }, {
            headers: {
                Authorization: "jwt " + token
            }
        })
    }
})

export default article