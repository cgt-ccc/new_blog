import http from '../utils/http'
import { reactive } from "vue";
import store from "../store/index.js"

const collect_article = reactive({
    collect_title: "",
    collect_content: "",
    collect_raw_content: "",
    collect_publish_time: "",
    collect_category: "",
    collect_article_num: 0,
    collect_author: "",
    collect_author_name: "",
    author_name: "",
    is_collect: false,
    create_collect_article() {
        return http.post(`/users/collect_article/`, {
            'collect_title': this.collect_title,
            'collect_content': this.collect_content,
            'collect_raw_content': this.collect_raw_content,
            'collect_category': this.collect_category,
            'collect_author_name': this.collect_author_name,
            'author_name': this.author_name,
            'collect_author': this.collect_author
        })
    },
    get_collect_article() {
        return http.get('/users/show_collect_article/')
    },
    delete_collect_article() {
        return http.delete(`/users/delete_collect_article/${this.article_collect_id}/`)
    }
})

export default collect_article