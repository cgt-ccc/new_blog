from rest_framework.generics import ListAPIView
from .models import Nav
from .serializers import NavModelSerializer
import constans


class NavHeaderAPIview(ListAPIView):
    queryset = Nav.objects.filter(position=constans.NAV_HEADER_POSITION, is_show=True, is_deleted=False).order_by("orders", "id")[:constans.NAV_HEADER_SIZE]
    serializer_class = NavModelSerializer
