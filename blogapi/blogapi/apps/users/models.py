from django.db import models
from django.contrib.auth.models import AbstractUser
from mdeditor.fields import MDTextField


# 列表
# from django.contrib.postgres.fields import ArrayField

# Create your models here.


class User(AbstractUser):
    mobile = models.CharField(max_length=15, unique=True, verbose_name="手机号")
    avatar = models.ImageField(upload_to="avatar/%Y", null=True, default="", verbose_name="个人头像")
    nickname = models.CharField(max_length=50, default="", null=True, verbose_name="用户昵称")
    description = models.TextField(default="")
    article_num = models.BigIntegerField(verbose_name="用户发布文章数", default=0)

    class Meta:
        db_table = 'blog_user'
        verbose_name = '用户信息'
        verbose_name_plural = verbose_name


class Details(models.Model):
    # 所处行业
    industry = models.CharField(max_length=32, verbose_name="所处行业", default="暂未设置，这里空空如也~")
    # 内容方向
    content_direction = models.CharField(max_length=32, verbose_name="内容方向", default="暂未设置，这里空空如也~")
    # github地址
    git_address = models.CharField(max_length=255, verbose_name="github地址", default="暂未设置，这里空空如也~")
    # 设置与用户表关联外键 一对多外键设置在多的模型中
    user = models.ForeignKey("User", on_delete=models.CASCADE, related_name="user_id")

    class Meta:
        db_table = 'Details'
        verbose_name = '用户详细资料'
        verbose_name_plural = verbose_name


# 发布内容
class Article(models.Model):
    title = models.CharField(max_length=255, verbose_name='文章标题')
    raw_content = MDTextField()  # 原生文章内容
    content = MDTextField()  # 使用markdown
    publish_time = models.DateTimeField(auto_now_add=True, verbose_name='发布时间')
    # article_num = models.BigIntegerField()
    category = models.CharField(max_length=255, verbose_name='类别', default='')
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='作者')
    picture = models.ImageField(upload_to="avatar/%Y", null=True, default="", verbose_name="文章封面")
    is_collect = models.BooleanField(default=False)
    collect_list = models.TextField(verbose_name="收藏者列表", default='', blank=True)
    look = models.BigIntegerField(verbose_name="浏览数", default=0)

    # collect_time = models.DateTimeField(auto_now_add=True, verbose_name='')

    class Meta:
        db_table = 'Article'
        verbose_name = '发布文章内容'
        verbose_name_plural = verbose_name


# 发表评论
class Comment(models.Model):
    article = models.ForeignKey(to=Article, verbose_name="评论文章", on_delete=models.DO_NOTHING)
    comment_content = models.TextField(verbose_name='评论内容')
    comment_author = models.ForeignKey(to=User, on_delete=models.DO_NOTHING, verbose_name='评论者')
    comment_time = models.DateTimeField(auto_now_add=True, verbose_name='评论时间')
    pre_comment = models.ForeignKey('self', on_delete=models.DO_NOTHING, null=True, verbose_name='父评论id',
                                    related_name="self")

    class Meta:
        db_table = 'Comment'
        verbose_name = '评论'
        verbose_name_plural = verbose_name


class Music(models.Model):
    music = models.FileField(upload_to="music/%Y", null=True, default="", verbose_name="音乐")
    music_picture = models.ImageField(upload_to="music_picture", null=True, default="", verbose_name="音乐封面")
    music_name = models.CharField(default='', verbose_name="歌曲名字", max_length=255)
    music_category = models.CharField(max_length=255, verbose_name='类别', default='music')
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='作者')
    collect_list = models.TextField(verbose_name="收藏者列表", default='', blank=True)
    publish_time = models.DateTimeField(auto_now_add=True, verbose_name='发布时间')


class Follow(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, verbose_name="关注的人", unique=True)


class Video(models.Model):
    video = models.FileField(upload_to="video/%Y", null=True, default="", verbose_name="视频")
    video_picture = models.ImageField(upload_to="video_picture", null=True, default="", verbose_name="视频封面")
    video_name = models.CharField(default='', verbose_name="视频名称", max_length=255)
    video_category = models.CharField(max_length=255, verbose_name='类别', default='video')
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='作者')
    collect_list = models.TextField(verbose_name="收藏者列表", default='', blank=True)
    publish_time = models.DateTimeField(auto_now_add=True, verbose_name='发布时间')
