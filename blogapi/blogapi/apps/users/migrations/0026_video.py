# Generated by Django 3.2.9 on 2024-02-29 10:15

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0025_music_publish_time'),
    ]

    operations = [
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('video', models.FileField(default='', null=True, upload_to='music/%Y', verbose_name='视频')),
                ('video_picture', models.ImageField(default='', null=True, upload_to='music_picture', verbose_name='视频封面')),
                ('video_name', models.CharField(default='', max_length=255, verbose_name='视频名称')),
                ('video_category', models.CharField(default='music', max_length=255, verbose_name='类别')),
                ('collect_list', models.TextField(blank=True, default='', verbose_name='收藏者列表')),
                ('publish_time', models.DateTimeField(auto_now_add=True, verbose_name='发布时间')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='作者')),
            ],
        ),
    ]
