import re

from rest_framework_jwt.views import ObtainJSONWebToken
from blogapi.utils.tencentcloudapi import TencentCloudAPI, TencentCloudSDKException
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveUpdateDestroyAPIView, UpdateAPIView, \
    ListCreateAPIView, RetrieveAPIView, DestroyAPIView

from .models import User, Details, Article, Comment, Follow, Music, Video

from .serializers import UserRegisterModelSerializer, UserDetailsSerializers, UserInfoSerializer, UserUpdateSerializer, \
    CreateDetailsSerializers
from .serializers import UserUpdatePasswordSerializer, UserUpdateEmailSerializer, UserUpdateMobileSerializer
from .serializers import UpdateIndustrySerializer, UpdateContentDirectionSerializer, UpdateGitAddressSerializer
from .serializers import UpdateDescriptionSerializer, ArticleCreateSerializer, showArticleSerializer
from .serializers import updateArticleCollectListSerializer
from .serializers import CreateCommentSerializer, GetChildCommentSerializer, AddLookNumSerializer
from .serializers import ReplyCommentSerializer, CreateChildCommentSerializer, GetLookNumSerializer, FollowSerializer, \
    DeleteFollowSerializer, GetMusicSerializer, UpdateMusicCollectSerializer, GetVideoSerializer
from .serializers import UpdateVideoCollectSerializer, DestroyMusicSerializer

# 权限判断
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly

import random
from django_redis import get_redis_connection
from django.conf import settings
from ronglianyunapi import send_sms


# Create your views here.


# class LoginAPIView(ObtainJSONWebToken):
#     """
#     用户登录视图
#     """
#
#     def post(self, request, *args, **kwargs):
#         try:
#             api = TencentCloudAPI()
#             result = api.captcha(
#                 request.data.get("ticket"),
#                 request.data.get("randstr"),
#                 request._request.META.get("REMOTE_ADDR"),
#             )
#             if result:
#                 print("验证通过")
#                 return super().post(request, *args, **kwargs)
#             else:
#                 raise TencentCloudSDKException
#         except TencentCloudSDKException as err:
#             return Response({"error_message": "验证码验证失败"}, status=status.HTTP_400_BAD_REQUEST)


class AccountAPIView(APIView):
    def get(self, request, mobile):
        try:
            val_mobile = str(User.objects.get(mobile=mobile))
            pattern = r'^1[3-9]\d{9}$'
            if re.match(pattern, val_mobile):
                return Response({"errmsg": "当前手机号已注册"}, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"errmsg": "手机号格式不正确"}, status=status.HTTP_400_BAD_REQUEST)
        except User.DoesNotExist:
            return Response({"errmsg": "OK"}, status=status.HTTP_200_OK)


class RegisterAPIView(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserRegisterModelSerializer


"""
/users/sms/(?P<mobile>1[3-9]\d{9})
"""


class SMSAPIView(APIView):
    """
    SMS短信接口视图
    """

    def get(self, request, mobile):
        """发送短信验证码"""
        redis = get_redis_connection("sms_code")
        # 判断手机短信是否处于发送冷却中[60秒只能发送一条]
        interval = redis.ttl(f"interval_{mobile}")  # 通过ttl方法可以获取保存在redis中的变量的剩余有效期
        if interval != -2:
            return Response(
                {"errmsg": f"短信发送过于频繁，请{interval}秒后再次点击获取!"},
                status=status.HTTP_400_BAD_REQUEST
            )

        # 基于随机数生成短信验证码
        # code = "%06d" % random.randint(0, 999999)
        code = f"{random.randint(0, 999999):06d}"
        # 获取短信有效期的时间
        time = settings.RONGLIANYUN.get("sms_expire")
        # 短信发送间隔时间
        sms_interval = settings.RONGLIANYUN["sms_interval"]
        # 调用第三方sdk发送短信
        send_sms(settings.RONGLIANYUN.get("reg_tid"), mobile, datas=(code, time // 60))

        # 记录code到redis中，并以time作为有效期
        # 使用redis提供的管道对象pipeline来优化redis的写入操作[添加/修改/删除]
        pipe = redis.pipeline()
        pipe.multi()  # 开启事务
        pipe.setex(f"sms_{mobile}", time, code)
        pipe.setex(f"interval_{mobile}", sms_interval, "_")
        pipe.execute()  # 提交事务，同时把暂存在pipeline的数据一次性提交给redis

        return Response({"errmsg": "OK"}, status=status.HTTP_200_OK)


# 新建详细资料
class CreateDetailsAPIView(CreateAPIView):
    queryset = Details.objects.all()
    serializer_class = CreateDetailsSerializers


class DetailsAPIView(RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserInfoSerializer
    # def get(self, request, pk):
    #     instance = User.objects.get(pk=pk)
    #     # instance = user.details_set.first()
    #     # print(instance)
    #     serializer = UserInfoSerializer(instance=instance)
    #     return Response(serializer.data)


"""
修改与监听数据
"""


# 修改用户名
class NicknameInfoUpdateAPIView(UpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserUpdateSerializer


# 修改密码
class PasswordUpdateAPIView(UpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserUpdatePasswordSerializer


class EmailUpdateAPIView(UpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserUpdateEmailSerializer


class MobileUpdateAPIView(UpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserUpdateMobileSerializer


class DescriptionAPIView(UpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UpdateDescriptionSerializer


"""
修改详细资料
"""


# 修改industry
class IndustryUpdateAPIView(UpdateAPIView):
    queryset = Details.objects.all()
    serializer_class = UpdateIndustrySerializer


# 修改content_direction
class ContentDirectionAPIView(UpdateAPIView):
    queryset = Details.objects.all()
    serializer_class = UpdateContentDirectionSerializer


class GitAddressAPIView(UpdateAPIView):
    queryset = Details.objects.all()
    serializer_class = UpdateGitAddressSerializer


"""
添加发布文章
"""


class ArticleCreateAPIView(CreateAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleCreateSerializer


class showArticle(ListAPIView):
    queryset = Article.objects.all()
    serializer_class = showArticleSerializer


# 创建文章列表
class ArticleAPIView(APIView):
    # queryset = Article.objects.all()
    # serializer_class = ArticleSerializer
    permission_classes = [IsAuthenticated]

    def post(self, request):
        picture = request.FILES.get("picture")
        title = request.data.get("title")
        content = request.data.get("content")
        category = request.data.get("category")
        author_id = request.data.get("author")
        raw_content = request.data.get('raw_content')

        if picture is None:
            return Response({"errmsg": "封面不能为空"}, status.HTTP_400_BAD_REQUEST)
        elif category == "":
            return Response({"errmsg": "文章类别不能为空"}, status.HTTP_400_BAD_REQUEST)
        elif title == "":
            return Response({"errmsg": "标题不能为空"}, status.HTTP_400_BAD_REQUEST)
        elif content == "":
            return Response({"errmsg": "文章内容不能为空"}, status.HTTP_400_BAD_REQUEST)

        # """
        # author_id :{
        #      picture: '',
        #      ....
        #  }
        # """
        #
        # redis = get_redis_connection("article_list")
        # redis.hset(f"{author_id}", picture, title, content, category, raw_content)

        instance = Article.objects.create(
            author_id=author_id,
            title=title,
            content=content,
            category=category,
            picture=picture,
            raw_content=raw_content,
        )
        instance.save()

        return Response({"errmsg": "ok"}, status.HTTP_200_OK)
    # redis缓存存储发布文章


class ArticleUpdateAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def put(self, request, pk):
        instance = Article.objects.get(pk=pk)
        picture = request.FILES.get("picture")
        title = request.data.get("title")
        content = request.data.get("content")
        category = request.data.get("category")
        author_id = request.data.get("author")
        raw_content = request.data.get('raw_content')
        # 不能直接进行更新数据，uploads_to无法识别
        # Article.objects.filter(pk=pk).update(
        #     picture=picture,
        #     title=title,
        #     content=content,
        #     category=category,
        #     author_id=author_id,
        #     raw_content=raw_content
        # )
        if picture is not None:
            instance.picture = picture
        # else:
        #     return Response({"errmsg": "封面不能为空"}, status.HTTP_400_BAD_REQUEST)

        instance.title = title
        instance.content = content
        instance.category = category
        instance.author_id = author_id
        instance.raw_content = raw_content
        instance.save()

        return Response({"errmsg": "ok"}, status.HTTP_200_OK)

    def delete(self, request, pk):
        Article.objects.filter(pk=pk).delete()
        return Response({"essmsg": "删除成功"}, status.HTTP_200_OK)


"""
上传头像
"""


class UserUpdateAvatarAPIView(APIView):
    def put(self, request, pk):
        avatar = request.FILES.get("avatar")
        user = User.objects.get(pk=pk)
        if avatar is None:
            return Response({"errmsg": "上传头像不能为空"}, status.HTTP_400_BAD_REQUEST)
        else:
            user.avatar = avatar
        user.save()

        return Response({"essmsg": "ok"}, status.HTTP_200_OK)


# 更新收藏者列表

class UpdateArticleCollectListAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Article.objects.all()
    serializer_class = updateArticleCollectListSerializer
    permission_classes = [IsAuthenticated]


# 增加浏览数
class AddLookArticleNumAPIView(RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Article.objects.all()
    serializer_class = AddLookNumSerializer


class GetLookArticleNumAPIView(RetrieveAPIView):
    queryset = Article.objects.all()
    serializer_class = GetLookNumSerializer


"""
发表评论
"""


class CreateCommentAPIView(ListCreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CreateCommentSerializer


class GetChildCommentAPIView(ListAPIView):
    queryset = Comment.objects.all()
    serializer_class = GetChildCommentSerializer


class CreateChildCommentAPIView(CreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CreateChildCommentSerializer


class ReplyCommentAPIView(CreateAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Comment.objects.all()
    serializer_class = ReplyCommentSerializer


class RedisTestAPIView(APIView):
    def post(self, request):
        # input1 = request.data.get("input1")
        # input2 = request.data.get("input2")
        redis = get_redis_connection("article_list")
        input_id = request.data.get("id", None)
        user_id = request.data.get("user")
        """
        article_用户ID: {
            input_id: 1
        }
        """
        redis = get_redis_connection("RedisTest")
        redis.hset(f"article_{user_id}", input_id)


"""
关注列表
"""


class FollowAPIView(ListCreateAPIView):
    queryset = Follow.objects.all()
    serializer_class = FollowSerializer


class DeleteFollowAPIView(DestroyAPIView):
    queryset = Follow.objects.all()
    serializer_class = DeleteFollowSerializer


class CreateMusicAPIView(APIView):
    permission_classes = [IsAuthenticated]
    def post(self, request):
        music = request.FILES.get("music")
        music_picture = request.FILES.get("music_picture")
        music_name = request.data.get("music_name")
        author_id = request.data.get("author")

        if music_picture is None:
            return Response({"errmsg": "封面不能为空"}, status.HTTP_400_BAD_REQUEST)
        elif music is None:
            return Response({"errmsg": "音乐文件不能为空"}, status.HTTP_400_BAD_REQUEST)

        instance = Music.objects.create(
            music=music,
            music_picture=music_picture,
            music_name=music_name,
            author_id=author_id
        )

        instance.save()
        return Response({"errmsg": "ok"}, status.HTTP_200_OK)


class GetMusicAPIView(ListAPIView):
    queryset = Music.objects.all()
    serializer_class = GetMusicSerializer


class UpdateMusicCollectAPIView(UpdateAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Music.objects.all()
    serializer_class = UpdateMusicCollectSerializer


class CreateVideoAPIView(APIView):
    permission_classes = [IsAuthenticated]
    def post(self, request):
        video = request.FILES.get("video")
        video_picture = request.FILES.get("video_picture")
        video_name = request.data.get("video")
        author_id = request.data.get("author")

        if video_picture is None:
            return Response({"errmsg": "封面不能为空"}, status.HTTP_400_BAD_REQUEST)
        elif video is None:
            return Response({"errmsg": "音乐文件不能为空"}, status.HTTP_400_BAD_REQUEST)

        instance = Video.objects.create(
            video=video,
            video_picture=video_picture,
            video_name=video_name,
            author_id=author_id
        )

        instance.save()
        return Response({"errmsg": "ok"}, status.HTTP_200_OK)


class GetVideoAPIView(ListAPIView):
    queryset = Video.objects.all()
    serializer_class = GetVideoSerializer


class UpdateVideoCollectAPIView(UpdateAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Video.objects.all()
    serializer_class = UpdateVideoCollectSerializer


"""
删除音乐和视频
"""


class DestroyMusicAPIView(DestroyAPIView):
    queryset = Music.objects.all()
    serializer_class = DestroyMusicSerializer

