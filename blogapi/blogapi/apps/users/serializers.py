from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework_jwt.settings import api_settings
from .models import User, Details, Article, Comment, Follow, Music, Video
import re, constans
from django_redis import get_redis_connection
from encryption import Emcrypte


class UserRegisterModelSerializer(serializers.ModelSerializer):
    """
    注册序列化器
    """
    re_password = serializers.CharField(write_only=True, required=True)
    sms_code = serializers.CharField(min_length=4, max_length=6, required=True, write_only=True)
    token = serializers.CharField(read_only=True)

    class Meta:
        model = User
        fields = ["nickname", "mobile", "password", "re_password", "sms_code", "token"]
        extra_kwargs = {
            "nickname": {
                "required": True, "write_only": True
            },
            "mobile": {
                "required": True, "write_only": True
            },
            "password": {
                "required": True, "write_only": True
            },
        }

    def validate(self, data):
        """
        验证客户端数据
        """
        # 验证用户名
        nickname = data.get("nickname", None)
        if nickname is None:
            raise serializers.ValidationError(detail={"msg": "用户名不能为空"})

        try:
            User.objects.get(nickname=nickname)
            raise serializers.ValidationError(detail={"msg": "用户名已被注册"})
        except:
            pass

        if not re.match("^[A-Za-z]+$", nickname):
            raise serializers.ValidationError(detail={"msg": "用户昵称格式不正确"})

        # 验证手机号格式
        mobile = data.get("mobile", None)
        if not re.match("^1[3-9]\d{9}$", mobile):
            raise serializers.ValidationError(detail="手机号格式不正确！")

        # 确认密码是否正确
        password = data.get("password")
        re_password = data.get("re_password")
        if password != re_password:
            raise serializers.ValidationError(detail="密码与确认密码不正确")

        if len(password) < 6 or len(password) > 16:
            raise serializers.ValidationError(detail="密码必须在6~16个字符之间")

        # 手机号是否已经注册
        try:
            User.objects.get(mobile=mobile)
            raise serializers.ValidationError(detail="手机号已注册")
        except User.DoesNotExist:
            pass

        # 验证短信验证码
        # 从redis中提取短信
        redis = get_redis_connection("sms_code")
        code = redis.get(f"sms_{mobile}")
        if code is None:
            """获取不到验证码，则表示验证码已经过期了"""
            raise serializers.ValidationError(detail={"验证码失效或已过期！"}, code="sms_code")

        # 从redis提取的数据，字符串都是bytes类型，所以decode
        if code.decode() != data.get("sms_code"):
            raise serializers.ValidationError(detail="短信验证码错误！", code="sms_code")
        print(f"code={code.decode()}, sms_code={data.get('sms_code')}")
        # 删除掉redis中的短信，后续不管用户是否注册成功，至少当前这条短信验证码已经没有用处了
        redis.delete(f"sms_{mobile}")

        return data

    def create(self, validated_data):
        """
        保存用户信息
        :param validated_data:
        :return:
        """
        nickname = validated_data.get("nickname")
        mobile = validated_data.get("mobile")
        password = validated_data.get("password")
        user = User.objects.create_user(
            username=mobile,
            nickname=nickname,
            mobile=mobile,
            password=password,
            avatar=constans.DEFAULT_USER_AVATAR
        )

        # 注册成功以后，免登陆
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(user)
        user.token = jwt_encode_handler(payload)

        return user


class UserDetailsSerializers(serializers.ModelSerializer):
    user_name = serializers.CharField(source='user.nickname')

    class Meta:
        model = Details
        fields = "__all__"


class CreateDetailsSerializers(serializers.ModelSerializer):
    class Meta:
        model = Details
        fields = "__all__"


class UserInfoSerializer(serializers.ModelSerializer):
    # user_id = serializers.IntegerField(source='id')
    # user_detail = serializers.SerializerMethodField()
    # def get_user_detail(self, all_details):
    #     return {'id': all_details.id, 'industry': all_details.username}
    details_set = UserDetailsSerializers(source='user_id', read_only=True, many=True)

    # course_set = serializers.PrimaryKeyRelatedField(id=34, many=True)

    class Meta:
        model = User
        fields = "__all__"

    def create(self, validated_data):
        return User.objects.create(**validated_data)


class UserUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["nickname"]

    def validate(self, data):
        nickname = data.get("nickname")
        if nickname is None:
            raise serializers.ValidationError(detail={"errmsg": '用户名不能为空'})

        if not re.match("[\u4e00-\u9fa5a-zA-Z]+$", nickname):
            raise serializers.ValidationError(detail={"errmsg": '用户名格式不正确'})

        try:
            User.objects.get(nickname=nickname)
            raise serializers.ValidationError(detail={"errmsg": '该用户名已存在'})
        except User.DoesNotExist:
            pass

        return data


class UserUpdatePasswordSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["password"]

    def validate(self, data):
        password = data.get("password", None)

        if len(password) < 6 or len(password) > 16:
            raise serializers.ValidationError(detail={"errmsg": "密码必须在6~16个字符之间"})

        return data

    def update(self, instance, validated_data):
        if validated_data.get("password"):
            val_password = validated_data.get("password")

            new_password = Emcrypte(val_password=val_password)
            instance.password = new_password

        instance.save()
        return instance


class UserUpdateEmailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["email"]

    def validate(self, data):
        email = data.get("email")

        if email == "":
            raise serializers.ValidationError(detail={"email": '邮箱不能为空'})

        try:
            User.objects.get(email=email)
            raise serializers.ValidationError(detail={"mobile": '该邮箱已存在'})
        except User.DoesNotExist:
            pass

        return data


class UpdateDescriptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["description"]

    def validate(self, data):
        description = data.get("description")
        if description is None:
            raise serializers.ValidationError(detail={"description": "description不能空白！"})

        return data


class UserUpdateMobileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["mobile"]

    def validate(self, data):
        mobile = data.get("mobile")

        if not re.match("^1[3-9]\d{9}$", mobile):
            raise serializers.ValidationError(detail={"mobile": "手机号格式不正确！"})

        try:
            User.objects.get(mobile=mobile)
            raise serializers.ValidationError(detail={"mobile": '该手机号已存在'})
        except User.DoesNotExist:
            pass

        return data


class UpdateIndustrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Details
        fields = ["industry"]

    def validate(self, data):
        industry = data.get("industry")

        if industry is None:
            raise serializers.ValidationError(detail={"industry": "所在行业不能空白！"})

        return data


class UpdateContentDirectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Details
        fields = ["content_direction"]

    def validate(self, data):
        content_direction = data.get("content_direction")
        if content_direction is None:
            raise serializers.ValidationError(detail={"content_direction": "内容方向不能空白！"})

        return data


class UpdateGitAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Details
        fields = ["git_address"]

    def validate(self, data):
        git_address = data.get("git_address")
        if git_address is None:
            raise serializers.ValidationError(detail={"git_address": "github地址不能空白！"})

        if not re.match('^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$', str(git_address)):
            raise serializers.ValidationError(detail={"git_address": "邮箱格式不正确！"})

        return data


class ArticleCreateSerializer(serializers.ModelSerializer):
    article_num = serializers.SerializerMethodField()

    class Meta:
        model = Article
        fields = "__all__"


class showArticleSerializer(serializers.ModelSerializer):
    # name = serializers.CharField(source='user.nickname')
    author_name = serializers.CharField(source='author.nickname')
    author_avatar = serializers.CharField(source='author.avatar')

    class Meta:
        model = Article
        fields = "__all__"


class updateArticleCollectListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ['collect_list']


class AddLookNumSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ['look']


class GetLookNumSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ['look']


class getArticleNum(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["article_num"]


"""
发表评论
"""


class CreateCommentSerializer(serializers.ModelSerializer):
    comment_author_name = serializers.CharField(source='comment_author.nickname')
    comment_author_avatar = serializers.CharField(source='comment_author.avatar')

    class Meta:
        model = Comment
        fields = "__all__"


# 回复评论
class ReplyCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = "__all__"


# # 回复子评论
# class ReplyChildCommentSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Comment
#         fields = "__all__"


class GetCommentSerializer(serializers.ModelSerializer):
    child_author_name = serializers.CharField(source='comment_author.nickname')
    child_author_avatar = serializers.CharField(source='comment_author.avatar')

    class Meta:
        model = Comment
        fields = "__all__"


class GetChildCommentSerializer(serializers.ModelSerializer):
    child_comment = GetCommentSerializer(source='self', read_only=True, many=True)
    comment_author_name = serializers.CharField(source='comment_author.nickname', read_only=True)
    comment_author_avatar = serializers.CharField(source='comment_author.avatar', read_only=True)

    class Meta:
        model = Comment
        # fields = ["child_comment"]
        fields = "__all__"


class CreateChildCommentSerializer(serializers.ModelSerializer):
    child_comment = GetCommentSerializer(source='self', read_only=True, many=True)
    comment_author_name = serializers.CharField(source='comment_author.nickname', read_only=True)
    comment_author_avatar = serializers.CharField(source='comment_author.avatar', read_only=True)

    class Meta:
        model = Comment
        fields = "__all__"


class FollowSerializer(serializers.ModelSerializer):
    user_name = serializers.CharField(source='user.nickname', read_only=True)
    user_avatar = serializers.CharField(source='user.avatar', read_only=True)

    class Meta:
        model = Follow
        fields = "__all__"


class DeleteFollowSerializer(serializers.ModelSerializer):
    class Meta:
        model = Follow
        fields = "__all__"


class GetMusicSerializer(serializers.ModelSerializer):
    music_author = serializers.CharField(source='author.nickname', read_only=True)

    class Meta:
        model = Music
        fields = "__all__"


class UpdateMusicCollectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Music
        fields = ['collect_list']


"""
获取视频
"""


class GetVideoSerializer(serializers.ModelSerializer):
    video_author = serializers.CharField(source='author.nickname', read_only=True)

    class Meta:
        model = Video
        fields = "__all__"


class UpdateVideoCollectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = ['collect_list']


"""
补上删除操作
"""


class DestroyMusicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Music
        fields = "__all__"
