from django.urls import path, re_path
from rest_framework_jwt.views import obtain_jwt_token
from .views import AccountAPIView, RegisterAPIView, SMSAPIView, DetailsAPIView, NicknameInfoUpdateAPIView,\
    CreateDetailsAPIView
from .views import PasswordUpdateAPIView, EmailUpdateAPIView, MobileUpdateAPIView, IndustryUpdateAPIView
from .views import ContentDirectionAPIView, GitAddressAPIView, DescriptionAPIView, ArticleCreateAPIView, showArticle, \
    AddLookArticleNumAPIView
from .views import ArticleAPIView, ArticleUpdateAPIView, UserUpdateAvatarAPIView, UpdateArticleCollectListAPIView
from .views import CreateCommentAPIView, GetChildCommentAPIView, ReplyCommentAPIView, \
    CreateChildCommentAPIView, RedisTestAPIView, GetLookArticleNumAPIView, FollowAPIView, DeleteFollowAPIView
from .views import CreateMusicAPIView, GetMusicAPIView, UpdateMusicCollectAPIView, CreateVideoAPIView, GetVideoAPIView, \
    UpdateVideoCollectAPIView
from .views import DestroyMusicAPIView
from django.conf import settings
from django.views.static import serve   # 静态文件代理访问模块

urlpatterns = [
    path('login/', obtain_jwt_token, name="login"),
    # path('login/', LoginAPIView.as_view(), name="login"),
    re_path(r"^mobile/(?P<mobile>1[3-9]\d{9})/$", AccountAPIView.as_view()),
    path("register/", RegisterAPIView.as_view()),
    re_path(r'uploads/(?P<path>.*)', serve, {"document_root": settings.MEDIA_ROOT}),
    re_path(r"^sms/(?P<mobile>1[3-9]\d{9})/$", SMSAPIView.as_view()),
    # 详细资料
    re_path(r"^details/(?P<pk>\d+)/$", DetailsAPIView.as_view()),
    # 更新个人信息
    re_path(r"^update_nickname/(?P<pk>\d+)/$", NicknameInfoUpdateAPIView.as_view()),
    re_path(r"^update_password/(?P<pk>\d+)/$", PasswordUpdateAPIView.as_view()),
    re_path(r"^update_email/(?P<pk>\d+)/$", EmailUpdateAPIView.as_view()),
    re_path(r"^update_mobile/(?P<pk>\d+)/$", MobileUpdateAPIView.as_view()),
    re_path(r"^update_description/(?P<pk>\d+)/$", DescriptionAPIView.as_view()),
    re_path(r"^update_avatar/(?P<pk>\d+)/$", UserUpdateAvatarAPIView.as_view()),
    # 更新详细资料
    re_path(r"^update_industry/(?P<pk>\d+)/$", IndustryUpdateAPIView.as_view()),
    re_path(r"^update_content_direction/(?P<pk>\d+)/$", ContentDirectionAPIView.as_view()),
    re_path(r"^update_git_address/(?P<pk>\d+)/$", GitAddressAPIView.as_view()),
    # 创建详细资料
    re_path(r"^create_details/(?P<pk>\d+)/$", CreateDetailsAPIView.as_view()),

    # path('create_article/', ArticleCreateAPIView.as_view()),
    # re_path(r"^article/(?P<pk>\d+)/$", ArticleCreateAPIView.as_view()),

    path('show_article/', showArticle.as_view()),
    path('article/', ArticleAPIView.as_view()),
    re_path(r"^update_article/(?P<pk>\d+)/$", ArticleUpdateAPIView.as_view()),
    # collect
    # path('collect_article/', ArticleCollectionAPIView.as_view()),
    # path('show_collect_article/', ShowArticleCollectionAPIView.as_view()),
    # re_path(r"^delete_collect_article/(?P<pk>\d+)/$", DeleteArticleCollectionAPIView.as_view()),
    # 更新浏览数
    re_path(r"^add_look_num/(?P<pk>\d+)/$", AddLookArticleNumAPIView.as_view()),
    # 获取浏览数
    re_path(r"^get_look_num/(?P<pk>\d+)/$", GetLookArticleNumAPIView.as_view()),
    # 更新文章收藏者列表
    re_path(r"^update_collect_list/(?P<pk>\d+)/$", UpdateArticleCollectListAPIView.as_view()),

    # 发表评论
    path('create_comment/', CreateCommentAPIView.as_view()),
    # 获取子级评论
    # re_path(r"^get_child_comment/(?P<pk>\d+)/$", GetChildCommentAPIView.as_view()),
    path('get_child_comment/', GetChildCommentAPIView.as_view()),
    path('create_child_comment/', CreateChildCommentAPIView.as_view()),
    path('reply_comment/', ReplyCommentAPIView.as_view()),

    # 测试redis
    path('redis_test/', RedisTestAPIView.as_view()),
    # 关注列表
    path('follow/', FollowAPIView.as_view()),
    path('get_follow/', FollowAPIView.as_view()),
    re_path(r"^delete_follow/(?P<pk>\d+)/$", DeleteFollowAPIView.as_view()),
    # music
    path('create_music/', CreateMusicAPIView.as_view()),
    path('get_music/', GetMusicAPIView.as_view()),
    re_path(r"^update_music_collect/(?P<pk>\d+)/$", UpdateMusicCollectAPIView.as_view()),
    re_path(r"^delete_music/(?P<pk>\d+)/$", DestroyMusicAPIView.as_view()),
    # video
    path('create_video/', CreateVideoAPIView.as_view()),
    path('get_video/', GetVideoAPIView.as_view()),
    re_path(r"^update_video_collect/(?P<pk>\d+)/$", UpdateVideoCollectAPIView.as_view()),
]

