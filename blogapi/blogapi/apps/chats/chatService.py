from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json

user_list = []


class chatService(WebsocketConsumer):
    """
    处理私信websocket请求
    """
    def connect(self):
        self.room_name = 'chatRoom'
        self.room_group_name = 'dweb'
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()

    def disconnect(self, code):
        """
        离开聊天组
        """
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    def receive(self, text_data=None, bytes_data=None):
        """
        接收到后端发来的私信
        """
        text_data_json = json.loads(text_data)
        print("接受用户信息", text_data_json)

        message = chat_code_to_userInfo(text_data_json['code'], text_data_json['msg'])

        event = {
            'type': 'chat_message',
            'message': message
        }

        async_to_sync(self.channel_layer.group_send)(self.room_group_name, event)

    def chat_message(self, event):
        message = event['message']
        self.send(text_data=json.dumps(message))


def chat_code_to_userInfo(code, msg):
    global user_list
    if code == 100:
        user = msg
        user_item = {
            'id': user['user_id'],
            'username': user['nickname'],
            'avatar': user['avatar']
        }
        if user_item not in user_list:
            user_list.append(user_item)

        res = {
            'code': 100,
            'userList': user_list
        }

        return res

    if code == 888:
        user = msg
        user_item = {
            'id': user['user_id'],
            'username': user['nickname'],
            'avatar': user['avatar']
        }
        if user_item in user_list:
            user_list.remove(user_item)

        res = {
            'code': 888,
            'userList': user_list
        }

        return res

    if code == 200:
        user = msg['user']
        res = {
            'code': 200,
            'msg': {
                'username': user['nickname'],
                'text': msg['text'],
                'avatar': user['avatar']
            }
        }

        return res
