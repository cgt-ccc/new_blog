from channels.routing import ProtocolTypeRouter, URLRouter
# from .channelsmiddleware import JwtAuthMiddleware
from channels.auth import AuthMiddlewareStack
from .urls import websocket_url

application = ProtocolTypeRouter({
    "websocket": AuthMiddlewareStack(
        URLRouter(
            websocket_url
        )
    )
})



