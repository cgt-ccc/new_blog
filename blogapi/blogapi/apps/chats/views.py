from django.shortcuts import render
from rest_framework.views import APIView
from blogapi.settings.dev import AUTH_USER_MODEL

# Create your views here.
"""
聊天get请求返回两用户聊天记录
"""
class ChatAPIView(APIView):
    def get(self, request):
        username = AUTH_USER_MODEL.objects.get()