from rest_framework_jwt.utils import jwt_payload_handler as payload_handler
from django.contrib.auth.backends import UserModel, ModelBackend
from django.db.models import Q


def jwt_payload_handler(user):
    """
    自定义载荷信息
    :params user  用户模型实例对象
    """
    # 先让jwt模块生成自己的载荷信息
    payload = payload_handler(user)
    if hasattr(user, 'avatar'):
        payload['avatar'] = user.avatar.url if user.avatar else ""

    if hasattr(user, 'nickname'):
        payload['nickname'] = user.nickname if user.nickname else ""

    if hasattr(user, 'mobile'):
        payload['mobile'] = user.mobile if user.mobile else ""

    if hasattr(user, 'description'):
        payload['description'] = user.description if user.description else ""

    return payload


def get_user(account):
    user = UserModel.objects.filter(Q(mobile=account) | Q(username=account) | Q(email=account)).first()
    return user


# 重写认证方法
class CustomAuthBackend(ModelBackend):
    """
    自定义用户认证类[实现多条件登录]
    """
    def authenticate(self, request, username=None, password=None, **kwargs):
        """
        多条件认证方法
        :param request: 本次客户端的http请求对象
        :param username:  本次客户端提交的用户信息，可以是user，也可以mobile或其他唯一字段
        :param password: 本次客户端提交的用户密码
        :param kwargs: 额外参数
        :return:
        """
        if username is None:
            # 获取用户信息，**kwargs作为函数参数名称来表示该参数是一个可变数量的关键字参数。
            username = kwargs.get(UserModel.USERNAME_FIELD)

        if username is None or password is None:
            return

        user = get_user(username)
        if user and user.check_password(password) and self.user_can_authenticate(user):
            return user


