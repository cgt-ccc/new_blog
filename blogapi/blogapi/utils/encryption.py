from django.utils import crypto
import hashlib
import base64


def Emcrypte(val_password):
    salt = None
    password = val_password
    algorithm = "pbkdf2_sha256"
    iterations = 260000
    digest = hashlib.sha256

    # 加密方法
    hash = crypto.pbkdf2(password, salt, iterations, digest=digest)
    hash = base64.b64encode(hash).decode('ascii').strip()

    new_password = "%s$%d$%s$%s" % (algorithm, iterations, salt, hash)

    return new_password

