"""常量配置文件"""
# 导航的位置 --> 顶部
NAV_HEADER_POSITION = 0
# 导航的位置 --> 开发者信息
NAV_DEVELOP_POSITION = 1
# 导航位置 --> 登陆后的个人信息导航
NAV_LOGIN_POSITION = 2

# 顶部导航显示的最大数量
NAV_HEADER_SIZE = 5

# 默认头像
DEFAULT_USER_AVATAR = "avatar/2024/avatar.png"
