import base64, json, time, hashlib

# JWT就一段字符串，由三段信息构成的，将这三段信息文本用`.`拼接一起就构成了Jwt token字符串
# 第一部分我们称它为头部（header)，第二部分我们称其为载荷（payload, 类似于飞机上承载的物品)，第三部分是签证（signature).

"""
-- 标准声明
- iss: jwt签发者
- sub: jwt所面向的用户
- aud: 接收jwt的一方
- exp: jwt的过期时间，这个过期时间必须要大于签发时间
- nbf: 定义在什么时间之后，该jwt才可以使用
- iat: jwt的签发时间
- jti: jwt的唯一身份标识，主要用来作为一次性token, 从而回避重放攻击。
"""

if __name__ == '__main__':
    '''jwt的头部生成'''
    header_data = {
        "typ": "jwt",
        "alg": "HS256"
    }
    header_json = json.dumps(header_data).encode()
    jwt_header = base64.b64encode(header_json).decode()
    # eyJ0eXAiOiAiand0IiwgImFsZyI6ICJIUzI1NiJ9
    # print(jwt_header)

    '''jwt的载荷生成'''
    iat = int(time.time())
    payload_data = {
        "sub": "root",
        "exp": iat + 3600,      # 过期时间一小时
        "iat": iat,
        # 公共声明
        "name": "cgt",
        "avatar": "1.png",
        "user_id": 1,
        "admin": True,
        # 私有声明（RSA非对称加密算法）:
        "acc_pwd": "QiLCJhbGciOiJIUzI1NiJ9QiLCJhbGciOiJIUzI1NiJ9QiLCJhbGciOiJIUzI1NiJ9"
    }

    payload_json = json.dumps(payload_data).encode()
    payload = base64.b64encode(payload_json).decode()
    # eyJzdWIiOiAicm9vdCIsICJleHAiOiAxNzA1Njc2MjA1LCAiaWF0IjogMTcwNTY3MjYwNSwgIm5hbWUiOiAiY2d0IiwgImF2YXRhciI6ICIxLnBuZyIsICJ1c2VyX2lkIjogMSwgImFkbWluIjogdHJ1ZSwgImFjY19wd2QiOiAiUWlMQ0poYkdjaU9pSklVekkxTmlKOVFpTENKaGJHY2lPaUpJVXpJMU5pSjlRaUxDSmhiR2NpT2lKSVV6STFOaUo5In0=
    # print(payload)

    # from django.conf import settings
    # secret = settings.SECRET_KEY
    secret = 'django-insecure-hbcv-y9ux0&8qhtkgmh1skvw#v7ru%t(z-#chw#9g5x1r3z=$p'
    data = jwt_header + payload + secret    # 秘钥不能提供客户端
    HS256 = hashlib.sha256()
    HS256.update(data.encode('utf-8'))

    signature = HS256.hexdigest()
    jwt_token = f"{jwt_header}.{payload}.{signature}"
    # eyJ0eXAiOiAiand0IiwgImFsZyI6ICJIUzI1NiJ9.eyJzdWIiOiAicm9vdCIsICJleHAiOiAxNzA1Njc2OTIzLCAiaWF0IjogMTcwNTY3MzMyMywgIm5hbWUiOiAiY2d0IiwgImF2YXRhciI6ICIxLnBuZyIsICJ1c2VyX2lkIjogMSwgImFkbWluIjogdHJ1ZSwgImFjY19wd2QiOiAiUWlMQ0poYkdjaU9pSklVekkxTmlKOVFpTENKaGJHY2lPaUpJVXpJMU5pSjlRaUxDSmhiR2NpT2lKSVV6STFOaUo5In0=.ae5de45e14a0f51306ebf304bfc204651c99238794c8b1103d7c66151cd4b195
    # print(jwt_token)

    header, payload1, signature1 = jwt_token.split(".")
    # 验证是否过期
    # 先基于base64，接着使用json解码
    payload_data = json.loads(base64.b64decode(payload1.encode()))
    # print(payload_data)
    exp = payload_data.get("exp", None)
    if exp is None and int(exp) < int(time.time()):
        print("token过期")
    else:
        print("token没有过期")

    # 验证token是否有效，是否篡改
    secret = 'django-insecure-hbcv-y9ux0&8qhtkgmh1skvw#v7ru%t(z-#chw#9g5x1r3z=$p'
    data = jwt_header + payload + signature
    HS256 = hashlib.sha256()
    HS256.update(data.encode('utf-8'))
    new_signature = HS256.hexdigest()

    if new_signature != signature:
        print("认证失败")
    else:
        print("认证成功")

    from django.utils import crypto
    import hashlib
    import base64

    salt = None
    password = 'liuyage20230811'
    algorithm = "pbkdf2_sha256"
    iterations = 120000
    digest = hashlib.sha256

    # 加密方法
    hash = crypto.pbkdf2(password, salt, iterations, digest=digest)
    hash = base64.b64encode(hash).decode('ascii').strip()

    password = "%s$%d$%s$%s" % (algorithm, iterations, salt, hash)
    print(password)
    print("%s$%d$%s$%s" % (algorithm, iterations, salt, hash))
